gl-forum-bugs-entry
=======================

Adds a bugs entry under categories at the forum.

Configuration
=====

From Admin > Site Settings > Plugin, modify `gl_bugs_button_url`.

Installation
============

* Add the plugin’s repository url to your container’s app.yml file

```yml
hooks:
  after_code:
    - exec:
        cd: $home/plugins
        cmd:
          - git clone https://github.com/discourse/docker_manager.git
          - git clone https://gitlab.com/LouFeng/gl-forum-bugs-entry.git
```

* Rebuild the container

```
cd /var/discourse
./launcher rebuild app
```